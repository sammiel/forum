<?php
	session_start();
	require_once "./includes/posts_handler.php"
?>
<!DOCTYPE html>
<html>
<head>
	<title>Chat</title>
	<script type="text/javascript" src="./js/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
</head>
<body>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#"><?php echo strtoupper($_SESSION["username"]); ?></a>
	    </div>
	    <!--<ul class="nav navbar-nav">
	      <li class="active"><a href="#">Home</a></li>
	      <li><a href="#">Page 1</a></li>
	      <li><a href="#">Page 2</a></li>
	      <li><a href="#">Page 3</a></li>
	    </ul>-->
	    <a href="./includes/logout.php" class="btn btn-info" style="float: right; position: relative; top: 8px;">Logout<span class="glyphicon glyphicon-log-out" style="margin-left: 3px;"></span></a>
	  </div>
	</nav>
	<div class="container">
		<div class="message_pane">
			<?php

				if (mysqli_num_rows($posts) > 0) {
				    while($row = mysqli_fetch_assoc($posts)) {
				        //echo $row["USERNAME"]. " " . $row["CONTENT"] . " " . $row["TIME_CREATED"] . "<br>";
			?>
				<div class="panel panel-info">
			      <div class="panel-heading">
			      	<?php echo strtoupper($row["USERNAME"]); ?>
			      	<span class="label label-info" style="float: right;">
			      		<?php echo strtoupper($row["TIME_CREATED"]); ?>
			      	</span>
		      	  </div>
			      <div class="panel-body"><?php echo $row["CONTENT"]; ?></div>
			    </div>

		    <?php      
			    }

				} else {
				    echo "No posts";
				}

			?>
		</div>
		<div class="well">
			<form class="form-group" role="form" method="post" action="">
			    <div class="form-group">
					 <input type="text" class="form-control" id="enter_text" name="content" autofocus="">
					 <input type="hidden" name="username" value="$_SESSION["username"]">
					 <input type="hidden" name="time" value="<?php echo (date("l"). " " . date("Y-m-d") . " " . date("h:i:sa"))?>">
					 <button type="submit" class="btn btn-success" style="width: 8%;"><span class="glyphicon glyphicon-send"></span></button>
			    </div>
		    </form>
		</div>
	</div>
</body>
</html>