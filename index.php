<?php
	session_start();
	include_once "./includes/db_connect.php";
	//include_once "./includes/login_handler.php";

	if (isset($_SESSION["sign_up_err_msg"])) {
		$err_msg = $_SESSION["sign_up_err_msg"];
	}else{
		$err_msg = "";
	}

	if(isset($_SESSION["sign_in_err_msg"])){
		$in_err_msg = $_SESSION["sign_in_err_msg"];
	}else{
		$in_err_msg = "";
	}
?>
<!DOCTYPE html>
<script type="text/javascript" src="./js/Chart.min.js"></script>
<script type="text/javascript" src="./js/scripts.js"></script>
<script type="text/javascript" src="./js/jquery.min.js"></script>
<script type="text/javascript">
	function refresh(){
		window.location.assign(window.location.href);
	}
	$('document').ready(function(){
		$("#form-header").animate({
			top: "+=200px"
		});

		$("#form-holder").animate({
			top: "200px"
		});

		$('#signupTrigger').click(function(){
			$("#form-header").text("SignUp");
			$("#loginForm").animate({
				top: '100px',
				opacity: '0'
			});
			$("#signupForm").show();
			$("#signupForm").animate({
				top: '-190px',
				opacity: '1'
			});			
			$("#form-holder").animate({
				height: '+=140px'
			});
			$("#form-holder").animate({
				top: "100px"
			});
		});

	});
</script>
<html>
<head>
	<title>Worka</title>
	<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body>
	<div id="form-holder" style="<?php if (($err_msg != "") || ($in_err_msg != "")) {echo 'padding-top: 48px; height: 270px;';} ?>">
		<div id="form-header">Login</div>
		<form action="./includes/login_handler.php" method="post" id="loginForm">
			<?php 
				if ($in_err_msg != "") {
			?>

			<div class="err_msg"><?php echo ($in_err_msg); ?></div>

			<?php 
				} 
				session_unset();
			?>
			<input type="text" name="username" class="inbox" placeholder="username">
			<input type="password" name="password" class="inbox" placeholder="password">
			<input type="submit" name="" value="Login" class="login_btn">
			<span id="signupTrigger">or Signup</span>
		</form>

		<form action="./includes/signup_work.php" method="post" id="signupForm">
			<?php if ($err_msg != "") {
					echo "<script>
							$('document').ready(function(){
								$('#signupTrigger').click();
							});
						</script>";
			?>

			<div class="err_msg"><?php echo ($err_msg); ?></div>

			<?php } 
				session_unset();
			?>
			<input type="text" name="email" class="inbox" placeholder="email">
			<input type="text" name="username" class="inbox" placeholder="username">
			<input type="password" name="password" class="inbox" placeholder="password">
			<input type="password" name="re_password" class="inbox" placeholder="re-enter password">
			<input type="submit" name="" value="Sign Up" class="login_btn">
			<span id="signupTrigger" onclick="refresh()">Login</span>
		</form>
	</div>

	<!--<div style="width: 500px; height: 500px;"><canvas id="myChart" width="400px" height="400px"></canvas></div>-->
<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

</body>
</html>
